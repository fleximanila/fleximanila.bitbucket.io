---
layout: page
title: readme
permalink: /readme/
---
[comment]: INSTRUCTIONS  //////////////////////////////////////////////////////////////

[comment]: run the server jekyll serve -H 0.0.0.0 -p 4000

[comment]: open this url: http://jekyll-fleximanila781610.codeanyapp.com:4000/readme

[comment]: ### DIGITAL MARKETING

[comment]: ### git clone fleximanila.bitbucket.io

[comment]: END //////////////////////////////////////////////////////////////

This email generator is created by *[jekyll](https://jekyllrb.com){:target="_blank"}*, there's a lot of resources online to understand jekyll but for ease of this documentation I will list down some helpful resources in the event that you will encounter confusing terms.

__[https://learn.cloudcannon.com/](https://learn.cloudcannon.com/){:target="_blank"}__  
__[https://www.taniarascia.com/make-a-static-website-with-jekyll/](https://www.taniarascia.com/make-a-static-website-with-jekyll/){:target="_blank"}__  
__[https://jekyllrb.com/](https://jekyllrb.com){:target="_blank"}__  
__[https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud){:target="_blank"}__  

If you skipped the tutorial above don’t worry I will still guide you through this process please use the list below for reference.

[Get the repository from the server and create new branch](#a){:style="color:#ed5f28;"}  
[Create new email template](#b){:style="color:#ed5f28;"}  
[How to build new template](#c){:style="color:#ed5f28;"}  
[Run the server](#d){:style="color:#ed5f28;"}  
[Convert your code to Email HTML](#e){:style="color:#ed5f28;"}  
[Brand switching](#f){:style="color:#ed5f28;"}  
[Adding new flexi brand](#g){:style="color:#ed5f28;"}  
[Assigning variables and use it to your templates](#h){:style="color:#ed5f28;"}  
[Liquid filters](#i){:style="color:#ed5f28;"}  
[UAT and CMC Variables](#j){:style="color:#ed5f28;"}  
[CSS Templates defaults](#k){:style="color:#ed5f28;"}  
[Force line break](#l){:style="color:#ed5f28;"}  
[Alignment](#m){:style="color:#ed5f28;"}

A. <a name="a">Get the repository from the server</a>

The `fleximanila.bitbucket.io` repository is hosted in bitbucket they have a free private repo unlike github they require you to upgrade your account. To make a copy just open your preferred IDE and load up your CLI then type;

```git clone https://bitbucket.org/fleximanila/fleximanila.bitbucket.io```

username: fleximanila@gmail.com  
password: Password1

### Create new git branch

Once cloned the development by default are pointed to `master` branch this is the main branch. Before you make version of your development it’s a best safe to make a new `branch`.

```git branch <new_branch>```

Your new line of development is now available, to access this new_branch just type;

```git checkout <new_branch>```

You are now good to go.

> search key word: __[Learn git branches](https://www.atlassian.com/git/tutorials/using-branches/){:target="_blank"}__

B. <a name="b">Create new email template</a>

All existing email templates are located in `post` folder.

> search key word: __[Jekyll file structure](https://learn.cloudcannon.com/jekyll/jekyll-file-structure/){:target="_blank"}__

``
~jekyll>_post/
``


Steps

1. Create new `post` file by pointing to post folder then right click to choose create file. The format would be "yyyy-mm-dd-title.html". Where YEAR is a four-digit number, MONTH and DAY are both two-digit numbers title is your preferred name and HTML is the file extension. For example:    
    ```
    2011-12-31-new-years-eve-is-awesome.html
    2012-09-12-how-to-write-a-blog.html
    ```

2. All email content must begin with YAML Front Matter. The YAML front matter is a variables on the page. For example:
{% highlight html %}
---
file_name: 01_FLR_E_Welcome_NewCustomer_OpsLease
category: Email-Projects
tags: [Re-branding]
---
{% endhighlight %}

> search key word: __[YAML front matter](https://learn.cloudcannon.com/jekyll/introduction-to-jekyll-front-matter/){:target="_blank"}__

The declared above YAML variables are used to organized the doc file in our `index.html`. Please access the below link for demo.

``http://jekyll-fleximanila781610.codeanyapp.com:4000``

Most likely the url address will not work. We will need to start our server.

Open your [terminal](#terminal){:style="color:#ed5f28;"} and type.

``
$ jekyll serve -H 0.0.0.0 -p 4000
``

Wait until your website completely generate. Then terminal will prompt...

``
Server running. . . press ctrl-c to stop.
``

Your website is now ready. Go back to your page then refresh the `http://jekyll-fleximanila781610.codeanyapp.com:4000`.

Our new created `post` file is now included to index.html!   

<br>

C. <a name="c">How to build new template</a>

This email templates is build through the help of __[foundation for email](http://foundation.zurb.com/emails.html){:target="_blank"}__ frameworks. The foundation email CSS frameworks are found in `~jekyll>_sass>module` folder,  `foundation-emails.scss`.
  
  Our templates are coded in foundation.
  
  This is how ROWS created.

{% highlight html %}
  <table align="center" class="container">
  <tbody>
    <tr>
      <td>
        <table class="row">
          <tbody>
            <th>
              Columns go here
            </th>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
{% endhighlight %}

This is how COLUMNS created.
   
Inside of the `<th>` is another full table. The content of your column goes inside of a `<th>`. Right below that table header should be another `<th>` with a class of .expander. This empty element helps the column expand to be full-width on small screens.

{% highlight html %}
<th class="small-12 large-12 columns first last">
  <table>
    <tr>
      <th>This is a column. Columns contain your content.</th>
      <th class="expander"></th>
    </tr>
  </table>
</th>
{% endhighlight %}

The above html code has ready to use css class. All foundation css class will found in `~jekyll>sass>module` folder, `foundation-emails.scss`.

I strongly recommend to visit this foundation __[grids](http://foundation.zurb.com/emails/docs/grid.html){:target="_blank"}__ documentation for complete guide on how to produce multiple responsive grids, text/image alignment, play around with columns/rows, offset, and collapsing.

You will find the additional custom css in `~jekyll>css>partials`. The 3 files there are additional specific css rules based on our email requirements. For example the `_brand-variables.scss`, the css code are filtered using __[sass](https://tutorialzine.com/2016/01/learn-sass-in-15-minutes){:target="_blank"}__, specific per flexi brand. This scss variables are updated based on the value from `flexi-brand` found in `~jekyll>_config.yml` file.

``_config.yml``

{% highlight yml %}
# Site settings
title: flex.marketing
email: your-email@domain.com
description: > # this means to ignore newlines until "baseurl:"
  Make your email re-branding easier, easy work means easy life.
baseurl: "" # the subpath of your site, e.g. /blog/
url: "" # the base hostname & protocol for your site

#FlexiCommercial #FlexiRent #EziLease #FlexiWay #SmartWay #Once #Lombard
flexi-brand: "Lombard"


#prod #uat
envi: uat 
envi_url_uat: https://flxuat.credagility.com
envi_url_prod: https://cmc.flexigroup.com.au

# Build settings
markdown: kramdown
{% endhighlight %}

`_custom-layout.scss` are intended for client that having their default css rule per preferred layout. If you are having trouble in colouring the link for Apple phone please add your fix there. Here the below sample code:

``_custom-layout.scss``

{% highlight scss %}
@media screen and (min-width: 401px ) and (max-width: $on-desktop) {
  @if $flexi-brand == "FlexiCommercial"{
    $primary-color: #38B449;
    .appleLinksPrimary-color a {color: $primary-color !important; text-decoration: none;}
  }
}

@media screen and (max-width: $on-mobile) {
  @if $flexi-brand == "FlexiCommercial"{
    $primary-color: #38B449;
    .appleLinksPrimary-color a {color: $primary-color !important; text-decoration: none;}
  }
}
{% endhighlight %}

HTML

{% highlight html %}
<span class='primary-color appleLinksPrimary-color'><strong>1300 340 447</strong></span>
{% endhighlight %}

The `_custom-style.scss` is there for css specific, fixes and overrides per brand and client. Here the below sample code:

Fix for outlook line height

``_custom-style.scss``

{% highlight scss %}
.outlookHeightFix {
   mso-line-height-alt:0;font-size:1px;line-height:0;
      p {
        mso-line-height-alt:0;font-size:1px;line-height:0 !important;
      }
}      
{% endhighlight %}

HTML
{% highlight html %}
 <table>
    <tr>
     <th class="outlookHeightFix" height="20" style="mso-line-height-alt:0;line-height:0;"></th>                                 </tr>
 </table>
{% endhighlight %}

``_custom-style.scss``

{% highlight scss %}
p.no-margin-bottom{
  Margin-bottom:0;
  margin-bottom:0;
}
{% endhighlight %}

HTML

{% highlight html %}
<p class="no-margin-bottom"><span class="secondary-color"><strong>Upgrade:</strong></span> if you would like to upgrade to new equipment at the end of your Rental Agreement, we may be able to provide a discount based on various factors including your loyalty to our business; or</p>
{% endhighlight %}

<br>

D. <a name="d">Run the server</a>

Container info
1. Right click the jekyll container>info. Look for the first url: `http://jekyll-fleximanila781610.codeanyapp.com`

<a name="terminal">Open terminal</a>

1. Right click the jekyll container>SSH terminal. Then type:

   ``jekyll serve -H 0.0.0.0 -p 4000``

2. Wait until the generated site are completed. Open the browser paste the url that you get from the container info. `http://jekyll-fleximanila781610.codeanyapp.com` then at the end add the port number `4000`. `http://jekyll-fleximanila781610.codeanyapp.com:4000`

3. To turn off the server just simply `ctrl + c` the terminal.

<br>

E. <a name="e">Convert your code to Email HTML</a>

Once you are done in your template. Locate your final rendered html source code and style.css.

1. Right click your browser page then view source code, copy your html and style.css code

2. Open the __[inliner](http://foundation.zurb.com/emails/inliner-v2.html){:target="_blank"}__ from foundation and fill the required HTML and CSS text box.

3. Uncheck the compress HTML. Then click the Inline button.

4. Copy the clipboard. You have now a ready to go Email HTML.

F. <a name="f">Brand switching</a>

`_config.yml` file holds the global variables of jekyll site.

``~jekyll>_config.yml``

We have `flexi-brand` and `envi` variables. Their values are changeable depend on the requirements.

Restart the server to take effect the new adjustment in `_config.yml`.

<br>

G. <a name="g">Adding new flexi brand</a>

``~jekyll>_includes>brand-controller.html``

This file are using control flow statement case and if else.

``brand-controller.html``

{% highlight liquid %}
{%raw%}{% case site.flexi-brand %}
  {% when 'FlexiCommercial' %}
    {% assign brand_name = "FlexiCommercial" %}
    {% if site.envi == "uat" %}
    <!-- assignment uat path for image, url, link, tel etc -->
    {% assign logo = {{site.envi_url_uat}} | append: "/fleximg/GetImage/images/FLC_E_Logo2017.png" %}
    {% elsif site.envi == "prod" %}
    <!-- assignment prod path for image, url, link, tel etc -->
    {% assign logo = {{site.envi_url_prod}} | append: "/fleximg/GetImage/images/FLC_E_Logo2017.png" %}
    {% endif %}
    
  {% when 'FlexiRent' %}
    {% assign brand_name = "FlexiRent" %}
    {% if site.envi == "uat" %}
    <!-- assignment uat path for image, url, link, tel etc -->
    {% elsif site.envi == "prod" %}
    <!-- assignment prod path for image, url, link, tel etc -->
    {% endif %}
    
   {% when 'EziLease' %}
    {% assign brand_name = "EziLease" %}
    {% if site.envi == "uat" %}
    <!-- assignment uat path for image, url, link, tel etc -->
    {% elsif site.envi == "prod" %}
    <!-- assignment prod path for image, url, link, tel etc -->
    {% endif %}
    
   {% when 'FlexiWay' %}
    {% assign brand_name = "FlexiWay" %}
    {% if site.envi == "uat" %}
    <!-- assignment uat path for image, url, link, tel etc -->
    {% elsif site.envi == "prod" %}
    <!-- assignment prod path for image, url, link, tel etc -->
    {% endif %}
    
   {% when 'SmartWay' %}
    {% assign brand_name = "SmartWay" %}
    {% if site.envi == "uat" %}
    <!-- assignment uat path for image, url, link, tel etc -->
    {% elsif site.envi == "prod" %}
    <!-- assignment prod path for image, url, link, tel etc -->
    {% endif %}
    
   {% when 'Lombard' %}
    {% assign brand_name = "Lombard" %}
    {% if site.envi == "uat" %}
    <!-- assignment uat path for image, url, link, tel etc -->
    {% elsif site.envi == "prod" %}
    <!-- assignment prod path for image, url, link, tel etc -->
    {% endif %}
    
    <!-- Below line is available for new brand -->
    
{% endcase %}{%endraw%}
{% endhighlight %}

> search key word: __[Control flow statement](https://learn.cloudcannon.com/jekyll/control-flow-statements-in-liquid/){:target="_blank"}__

You can easily include the `brand-controller.html` to the upper section of the template (below front matter). All assigned variables in `brand-controller.html` are now available to the working template. They are responsible for `html` elements changes.

{% highlight html %}
{% raw %}---
file_name: 01_FLR_E_Welcome_NewCustomer_OpsLease
category: Email-Projects
tags: [Re-branding]
---
{% include brand-controller.html %}

<!doctype html>
<table>
  ...
    ...<img class="small-float-center float-left flexi-logo" src="{{ logo }}" /> ...
  ...
</table>{% endraw %}
{% endhighlight %}

At the same time the `scss` also capture the site variables once the `_config.yml` has changed.

``~jekyll>css>partials>_brand_variables.scss``

Like our `brand-controller.html`, the `_brand_variables.scss` are updated `css` to the template.

``_brand_variables.scss``

{% highlight scss %}
@if $flexi-brand == "FlexiCommercial" {
/*FlexiCommercial*/
        $primary-color: #38B449;
        .primary-color{color: $primary-color;}
        .primary-bg-color{background: #38B449; color:#ffffff;}
        .secondary-color{color: #3E6683}
        .secondary-bg-color{background-color: #3E6683; color: #ffffff;}        
        a.link-color, .link-color{color: #CCCCCC !important;}        
        .text-color, .text-color table, .text-color td {color: $text-color;}
}

@if $flexi-brand == "FlexiRent"{
/*FlexiRent*/
        $primary-color: #E30450;
        .primary-color{color: $primary-color;}
        .primary-bg-color{background: #E30450; color:#ffffff;}
        .secondary-color{color: #00A0DF}
        .secondary-bg-color{background-color: #00A0DF; color: $text-color;}        
        a.link-color, .link-color{color: #00A3A1 !important;}        
        .text-color, .text-color table, .text-color td {color: $text-color;}
}

@if $flexi-brand == "EziLease" {
/*EziLease */
        $primary-color: #336699;  
        .primary-color{color: $primary-color;}
        .primary-bg-color{background: #336699; color:#ffffff;}
        .secondary-color{color: #55555;}
        .secondary-bg-color{background-color: #55555; color: $text-color;}        
        a.link-color, a.link-color:link, a.link-color:visited {color: #336699 !important;}        
        .text-color, .text-color table, .text-color td {color: $text-color;}
}

@if $flexi-brand == "FlexiWay" or $flexi-brand == "FlexiWay2" {
/*FlexiWay */            
        $primary-color: #00a0df;                   
        .primary-color{color: $primary-color;}
        .primary-bg-color{background: #00a0df; color:#ffffff;}
        .secondary-color{color: #6f2c91;}
        .secondary-bg-color{background-color: #6f2c91; color: #ffffff;}        
        a.link-color, .link-color{color: #f6ad1d !important;}        
        .text-color, .text-color table, .text-color td {color: $text-color;}
}
{% endhighlight %}

All `scss` files are arranged strategically in `~jekyll>css>style.scss`. Take a look at the bottom part.

{% highlight scss %}
@import
		"module/foundation-emails.scss",
		"../css/partials/custom-style",		
		"../css/partials/custom-layout",
		"../css/partials/brand-variables";
{% endhighlight %}

The first line `module/foundation-emails.scss` are the email `css` frameworks. The line are replaceable with preferred frameworks.

The second line `../css/partials/custom-style` is the customizable `css`. You can put your outlook hack there. The `css` rule will be inline into your `html` elements if `css` class is used.

The third line `../css/partials/custom-layout` is your customizable `css` layout. It can add or edits mobile hacks specially for Apple devices. The `css` rule will embed at html `<head>`, whether the `css` class will be used or not.

The fourth line `../css/partials/brand-variables` is the `css` brand controller.

<br>

H. <a name="h">Assigning variables and use it to your templates</a>

By default jekyll used three method to assign variables.

1. `_config.yml` a global variables available to all system like js, css/scss and html.
Assigning variables

   {% highlight yaml %}
   flexi-brand: "Lombard"
   {% endhighlight %}
   
   Calling variables
   
   If you need to use global variables just call the `site` then `.` followed by the actual variable name make sure to close it in double open and close braces. Sample below.
   
   {% highlight liquid %}
   {%raw%}{{site.flexi-brand}}{%endraw%}
   {% endhighlight %}
   
   HTML
   
   {% highlight html %}
   <p>{% raw %}{{site.flexi-brand}} {% endraw %}Customer Team</p>
   {% endhighlight %}
   
   Output:
   
   {{ site.flexi-brand }} Customer Team
   
   SCSS
   
   `~jekyll>css>style.scss`
   
   {% highlight scss %}
   $flexi-brand: {%raw%}{{ site.flexi-brand }}{%endraw%};
   {% endhighlight %}  

2. YAML Front Matter

   Front matter is used to set variables and metadata on pages in your Jekyll site.

   Assigning variables
   
   {% highlight html %}  
   ---
   file_name: 01_FLR_E_Welcome_NewCustomer_OpsLease
   ---
   {% endhighlight %}   

   Calling variables
   
   {% highlight html %}
   <title>{%raw%}{{ page.file_name }}{%endraw%}</title>
   {% endhighlight %}
      
   Output

   01_FLR_E_Welcome_NewCustomer_OpsLease

3. Liquid template language

   You can assign liquid variable in any part of your templates.

   Assigning variables
   
   {% highlight liquid %}   
   {%raw%}{% assign tel = "1300 340 447" %}{%endraw%}
   {% endhighlight %}
   
   Calling variables
   
   {% highlight html %}
   <a title="tel:{%raw%}{{ tel }}{%endraw%}" href="tel:{%raw%}{{ tel }}{%endraw%}"></a>
   {% endhighlight %}
      
   Output 
   
   [1300 340 447](1300 340 447){:title="tel:1300 340 447" href="tel:1300 340 447"}  

<br>

I. <a name="i">Liquid filters</a>

``{%raw%}<a title="tel:{{ tel | remove: ' ' }}" href="tel:{{ tel | remove: ' ' }}"><img class="float-right small-float-center" src="{{ tel_top_icon }}" /></a>{%endraw%}``

> search key word: __[Liquid string filters](https://learn.cloudcannon.com/jekyll/string-filters-in-liquid/){:target="_blank"}__

<br>

J. <a name="j">UAT and CMC Variables</a>

Collection of assigned uat and cmc variables.

``~jekyll>_includes>variables.html``

It has a liquid control statement flow.

{% highlight liquid %}
{%raw%}{% if site.envi == "uat" %}
  {% assign PayOutExpiryDate =            "${displayOpsEOTReturnDate}" %}
{% elsif site.envi == "prod" %}
  {% assign PayOutExpiryDate =            "${displayoLEOTPayoutExp}" %}
{% endif %}{%endraw%}
{% endhighlight %}

Just include the `variables.html` at upper section of your template after YAML Front Matter.

{% highlight html %}
{% raw %}---
file_name: 25_FLR_E_EarlyPurchaseExpire_OpLease
category: Email-Projects
tags: [Re-branding]
---
{% include brand-controller.html %}
{% include variables.html %}

<!doctype html>
<table>
  ...
    ...<p>We have <u>not</u> yet received the termination amount from you which was due <u>on or before</u> {{PayOutExpiryDate}}. As a result, our agreement to sell you the Equipment has now expired and the termination amount has changed. You must continue to pay each rental payment and any other amount due and payable under your Rental Agreement at the original payment schedule.</p> ...
  ...
</table>{% endraw %}
{% endhighlight %}

Output if the `_config.yml` variable `envi:` is equal to "__uat__"

``envi: uat``

<p>We have <u>not</u> yet received the termination amount from you which was due <u>on or before</u> <b>${displayOpsEOTReturnDate}</b>. As a result, our agreement to sell you the Equipment has now expired and the termination amount has changed. You must continue to pay each rental payment and any other amount due and payable under your Rental Agreement at the original payment schedule.</p>

Output if the `_config.yml` variable `envi:` is equal to "__prod__"

``envi: prod``

<p>We have <u>not</u> yet received the termination amount from you which was due <u>on or before</u> <b>${displayoLEOTPayoutExp}</b>. As a result, our agreement to sell you the Equipment has now expired and the termination amount has changed. You must continue to pay each rental payment and any other amount due and payable under your Rental Agreement at the original payment schedule.</p>

<br>

K. <a name="k">CSS Templates defaults</a>

``~jekyll>css>style.scss``

{% highlight scss %}
$base-font-family: Helvetica, Arial, sans-serif;

$text-color:       #555555; /* default text color */
$header:           14px; /* text size for `<h1>` */
$p:                12px; /* text size for `<p>` */
$tc:               10px; /* text size for terms & condition */
$line-height:      185%;
$footer-line-height: $line-height - 65;
$base-margin:      15px;

$on-desktop:       4096px; /* breakpoint for desktop */
$on-mobile:        596px; /* breakpoint for mobile */
{% endhighlight %}

<br>

L. <a name="l">Force line break</a>

If outlook don't recognize your line break for image, text, and block section, this patch would help, this works like a `<br>` but a custom `height` attribute instead. 

Break line or line space at the bottom of an image.

{% highlight html %}
{%raw%}<img src="image-url.png" alt="sample-image">
{% include line-height.html %}{%endraw%}
{% endhighlight %}

Here the source code.

HTML
{% highlight html %}
<table>
   <tr>
      <th class="outlookHeightFix" height="10" style="mso-line-height-alt:0;line-height:0;"></th>
   </tr>
</table>
{% endhighlight %}

You can control the spacing by chaging the value of `<th>` height attribute. Our CMC are supplying `<p>` element to all empty tags inside `<th>` and `<td>`. We need to work out our css, applying a default css height reset for `<p>`.

SCSS

``~jekyll>css>partials>_custom-style.scss``

{% highlight scss %}
.outlookHeightFix {
   mso-line-height-alt:0;font-size:1px;line-height:0;
      p {
         mso-line-height-alt:0;font-size:1px;line-height:0 !important;
      }
}
{% endhighlight %}

You will find this source code in our `~jekyll>_includes\` folder and look for the `line-height.html` file. Think of a block of a lego every piece are reuseable depend on what object you wanted to build. This is the exact way how this `_includes` folder works, all files there are reusable piece html block. You can also create your own html block.

This patch is also recognized in most major email client.

{:.callout .alert style="color:#908890;"}
*Do not use this patch after `<p>` because our `<p>` tag is by default is having 10px bottom-margin so it will results to a double line space. 10px for `<p>` and 10px for `height` attribute.*

<br>

M. <a name="m">Alignment</a>

Text Alignment

You can align text with the `.text-x` classes. These classes will appy to the large breakpoint as well as the small.

{% highlight html %}
<style>
  .columns {
    border: 1px solid #333;
  }
</style>
<table align="center" class="container">
  <tbody>
    <tr>
      <td>
        <table class="row">
          <tbody>
            <tr>
              <th class="small-12 large-4 columns first">
                <table>
                  <tr>
                    <th>
                      <p class="text-left">Left (default)</p>
                    </th>
                  </tr>
                </table>
              </th>
              <th class="small-12 large-4 columns">
                <table>
                  <tr>
                    <th>
                      <p class="text-center">center</p>
                    </th>
                  </tr>
                </table>
              </th>
              <th class="small-12 large-4 columns last">
                <table>
                  <tr>
                    <th>
                      <p class="text-right">right</p>
                    </th>
                  </tr>
                </table>
              </th>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
{% endhighlight %}

You can combine these classes to override the behavior on the small breakpoint.

{% highlight html %}
<style>
  .columns {
    border: 1px solid #333;
  }
</style>
<table align="center" class="container">
  <tbody>
    <tr>
      <td>
        <table class="row">
          <tbody>
            <tr>
              <th class="small-12 large-6 columns first">
                <table>
                  <tr>
                    <th>
                      <p class="text-center small-text-left">Center, small left</p>
                    </th>
                  </tr>
                </table>
              </th>
              <th class="small-12 large-6 columns last">
                <table>
                  <tr>
                    <th>
                      <p class="text-right small-text-center">Right, small center</p>
                    </th>
                  </tr>
                </table>
              </th>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
{% endhighlight %}

Centering Images

Just wrap the `<center>` around an image you’ll be good to go. Inky will handle the magic behind the scenes! In the CSS version, you’ll add a `.float-center` class, `align="center"` attribute and wrap a `<center>` tag to make sure things are centered.

{% highlight html %}
<table align="center" class="container">
  <tbody>
    <tr>
      <td>
        <table class="row">
          <tbody>
            <tr>
              <th class="small-12 large-12 columns first last">
                <table>
                  <tr>
                    <th>
                      <center data-parsed="">
                        <img src="http://placehold.it/200?text=center" alt="image of clever meme that made me chuckle" align="center" class="float-center">
                      </center>
                    </th>
                    <th class="expander"></th>
                  </tr>
                </table>
              </th>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
{% endhighlight %}

So to be clear: with Inky you only need to wrap an image in <center> tag to reliably center it.

In the CSS version, our centering recipe includes three ingredients:

  * `.float-center` class on the `<img>` element
  * `align="center"` attribute on the `<img>` element
  * Wrap the `<img>` with `<center>` tags (needed for Outlook 2007, 2010, and 2011)

Also, it's not really a float, but the `.float-center` class to an element to engage the automatic margin centering trick. Note that this will only work on elements with an absolute width, which means not a percentage or auto width.

If you need to center an image only on mobile clients, you can apply the `.small-float-center` class. A good use case would be centering an image smaller than the full column width on devices like an iPhone 5, iPhone 6, and Android 4.4.

{% highlight html %}
<table align="center" class="container">
  <tbody>
    <tr>
      <td>
        <table class="row">
          <tbody>
            <tr>
              <th class="large-offset-1 small-12 large-3 columns first">
                <table>
                  <tr>
                    <th>
                      <img class="small-float-center" src="http://placehold.it/200?text=small-center" alt="please don&apos;t forget me">
                    </th>
                  </tr>
                </table>
              </th>
              <th class="small-12 large-8 columns last">
                <table>
                  <tr>
                    <th>
                      <h4 class="small-text-center">What is the deal?</h4>
                      <p class="small-text-center">Sweet beast sun bathe or chase mice rub face on everything or leave dead animals as gifts for mark territory play time.</p>
                    </th>
                  </tr>
                </table>
              </th>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
{% endhighlight %}

Aligning Images Left and Right

You can also align images to the left or the right.

{% highlight html %}
<table align="center" class="container">
  <tbody>
    <tr>
      <td>
        <table class="row">
          <tbody>
            <tr>
              <th class="small-12 large-12 columns first last">
                <table>
                  <tr>
                    <th>
                      <img class="float-left" src="http://placehold.it/200?text=left" alt="">
                      <img class="float-center" src="http://placehold.it/200?text=center" alt="">
                      <img class="float-right" src="http://placehold.it/200?text=right" alt="">
                    </th>
                    <th class="expander"></th>
                  </tr>
                </table>
              </th>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
{% endhighlight %}

Vertical Alignment

You can vertically align your content within columns by using `valign` attribute. The available values are `top`, `middle`, and `bottom`.

{% highlight html %}
<table class="row">
  <tbody>
    <tr>
      <th class="small-12 large-3 columns first" valign="top">
        <table>
          <tr>
            <th>
              <img class="float-right" src="http://placehold.it/50" alt="">
            </th>
          </tr>
        </table>
      </th>
      <th class="small-12 large-3 columns" valign="bottom">
        <table>
          <tr>
            <th>
              <h4>Bottom</h4>
            </th>
          </tr>
        </table>
      </th>
      <th class="small-12 large-3 columns" valign="middle">
        <table>
          <tr>
            <th>
              <h4 style="margin-bottom: 0;" class="text-right">HEADLINE</h4>
              <p style="margin-bottom: 0;" class="text-right subheader">SUBHEADLINE</p>
            </th>
          </tr>
        </table>
      </th>
      <th class="small-12 large-3 columns last" valign="middle">
        <table>
          <tr>
            <th>
              <img class="small-float-center" src="http://placehold.it/250" alt="">
            </th>
          </tr>
        </table>
      </th>
    </tr>
  </tbody>
</table>
{% endhighlight %}

> search key word: __[Alignment](https://foundation.zurb.com/emails/docs/alignment.html){:target="_blank"}__


 {% include comments.html %}  

















